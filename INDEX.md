# ROMDSK

ROM disk toolkit to allow DOS kernel boot off ROM (EPROM, Flash)

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## ROMDSK.LSM

<table>
<tr><td>title</td><td>ROMDSK</td></tr>
<tr><td>version</td><td>1.1a</td></tr>
<tr><td>entered&nbsp;date</td><td>2004-10-27</td></tr>
<tr><td>description</td><td>Toolkit to allow DOS to boot off ROM</td></tr>
<tr><td>keywords</td><td>ROM disk EPROM flash EEPROM DOS FreeDOS kernel boot INT19H</td></tr>
<tr><td>author</td><td>sbk _at_ gifta.bg (Stanislav Bonchev)</td></tr>
<tr><td>maintained&nbsp;by</td><td>georgievli _at_ netscape.net (Luchezar Georgiev)</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://ibiblio.org/pub/micro/pc-stuff/freedos/files/util/system/</td></tr>
<tr><td>platforms</td><td>DOS (TASMB), FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>summary</td><td>ROM disk toolkit to allow DOS kernel boot off ROM (EPROM, Flash)</td></tr>
</table>
